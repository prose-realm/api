<?php

namespace App\Models\Gate;

use App\Models\Gate\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserRole extends Model
{
    use HasFactory;

    protected $table = 'gate.user_roles';
    protected $guarded = [];

    public function role(){
        return $this->belongsTo(Role::class);
    }
}
