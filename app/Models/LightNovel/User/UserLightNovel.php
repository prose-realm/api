<?php

namespace App\Models\LightNovel\User;

use App\Models\User;
use App\Models\LightNovel\Series;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserLightNovel extends Model
{
    use HasFactory;

    protected $table = "user_light_novel.series";

    protected $guarded= [];

    public function series(){
        return $this->belongsTo(Series::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function history(){
        return $this->hasMany(UserLightNovelHistory::class);
    }
}
