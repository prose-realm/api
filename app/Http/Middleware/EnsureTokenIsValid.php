<?php

namespace App\Http\Middleware;

use Closure;
use App\ResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */

    use ResponseController;

    public function handle(Request $request, Closure $next): Response
    {
        if (!auth('sanctum')->check()) {
            return $this->invalidToken(Route::currentRouteName());
        }
        return $next($request);
    }
}
