<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE SCHEMA IF NOT EXISTS user_light_novel");

        Schema::create('user_light_novel.series', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->default(DB::raw('uuid_generate_v4()'));
            $table->foreignId('user_id')->nullable()->index()->constrained('gate.users');
            $table->foreignId('series_id')->nullable()->index()->constrained('light_novel.series');
            $table->string('volume')->default(0);
            $table->string('chapter')->default(0);
            $table->string('part')->default(0);
            $table->string('status')->nullable();
            $table->string('score')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_light_novel.series');
    }
};
