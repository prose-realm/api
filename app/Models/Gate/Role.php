<?php

namespace App\Models\Gate;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'gate.roles';
    protected $guarded = [];
}
