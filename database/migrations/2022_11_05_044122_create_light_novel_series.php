<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE SCHEMA IF NOT EXISTS light_novel');

        Schema::create("light_novel.series", function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->default(DB::raw('uuid_generate_v4()'));
            $table->string('title');
            $table->longText('description')->nullable();
            $table->string('status')->nullable();
            $table->string('verif')->nullable();
            $table->timestampTz('release_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('light_novel.series');
    }
};
