<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("light_novel.chapters", function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->default(DB::raw('uuid_generate_v4()'));
            $table->foreignId('volume_id')->nullable()->index()->constrained('light_novel.volumes');
            $table->string('chapter_number')->nullable();
            $table->string('title')->nullable();
            $table->longText('content')->nullable();
            $table->timestampTz('release_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('light_novel.chapters');
    }
};
