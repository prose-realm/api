<?php

namespace App\Models\LightNovel;

use App\Models\LightNovel\User\UserLightNovel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Series extends Model
{
    use HasFactory;
    protected $table = "light_novel.series";

    protected $guarded = [];


    public function volume()
    {
        return $this->hasMany(Volume::class);
    }

    public function user_library()
    {
        return $this->hasMany(UserLightNovel::class);
    }
}
