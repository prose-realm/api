<?php

namespace App\Http\Controllers\v0\User\LightNovel;

use App\ResponseController;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\LightNovel\User\UserLightNovel;
use App\Http\Controllers\Controller;
use App\Models\LightNovel\Series;
use Illuminate\Support\Facades\Validator;

class SeriesController extends Controller
{
    use ResponseController;

    public function getAllUserLightNovel(){
        $user = auth('sanctum')->user();
        $data = UserLightNovel::where('id', $user->id)->exists();
        if($data){
            $data = UserLightNovel::where('user_id', $user->id)->get();
            return $this->success(__FUNCTION__, $data);
        }
        return $this->notFound(__FUNCTION__);
    }

    public function getUserLightNovel($id){
        $UserLightNovel = UserLightNovel::with('series')
                            ->whereRelation('series', 'uuid', $id)
                            ->where('user_id', auth('sanctum')->user()->id)
                            ->first();
        if ($UserLightNovel === null) {
            return $this->notFound(__FUNCTION__);
        }
        return $this->success(__FUNCTION__, $UserLightNovel);
    }

    public function addUserLightNovel(Request $request){
        $validation = Validator::make($request->all(), [
            'series_uuid' => 'required | exists:series,uuid'
        ]);

        if($validation->fails()){
            return $this->validationError(__FUNCTION__, $validation->errors());
        }

        $series = Series::where('uuid', $request->series_uuid)->first();
        if(!$series){
            return $this->validationError(__FUNCTION__, $validation->errors());
        }

        $UserLightNovel = UserLightNovel::with('series')
                            ->whereRelation('series', 'uuid', $request->series_uuid)
                            ->where('user_id', auth('sanctum')->user()->id)
                            ->first();
        if ($UserLightNovel === null) {
            $data = UserLightNovel::create([
                'user_id' => auth('sanctum')->user()->id,
                'series_id'=> $series->id,
                'volume' => 0,
                'chapter' => 0,
                'status'=> 'PTR',
                'score' => 0
            ]);
            return $this->success(__FUNCTION__, $data);
        }
        return $this->found(__FUNCTION__, $UserLightNovel);
    }

    public function updateUserLightNovel(Request $request){
        $validation = Validator::make($request->all(), [
            'series_uuid' => 'required',
            'volume' => 'required',
            'chapter' => 'required',
            'status'=> 'required',
            'score' => 'required'
        ]);

        if($validation->fails()){
            return $this->validationError(__FUNCTION__, $validation->errors());
        }
        $series = Series::where('uuid', $request->series_uuid)->first();
        if(!$series){
            return $this->validationError(__FUNCTION__, $validation->errors());
        }

        $UserLightNovel = UserLightNovel::where('user_id', auth('sanctum')->user()->id)->where('series_id', $series->id)->first();
        $UserLightNovel->update([
            'volume' => $request->volume,
            'chapter' => $request->chapter,
            'status'=> $request->status,
            'score' => $request->score
        ]);

        return $this->success(__FUNCTION__, $UserLightNovel);
    }
}
