<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\LightNovel\Series;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MyLnListToProseRealm extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $lnList = DB::connection('MyLnList')->table('lnList')->get();

        foreach ($lnList as $ln) {
            Series::updateOrCreate([
                "id" => $ln->id,
            ], [
                "title" => $ln->title,
                "description" => $ln->description,
                "status" => $ln->status,
                "verif" => $ln->verif
            ]);
        }

        //Get Last ID
        $id = DB::connection('MyLnList')->table('lnList')->orderBy('id', 'desc')->first()->id;
        $id = $id + 1;
        DB::statement("ALTER SEQUENCE light_novel.series_id_seq RESTART WITH $id");
    }
}
